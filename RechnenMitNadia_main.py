#!/usr/bin/python3
# encoding: utf-8

import sys
sys.path.append('./Common')

import time
import argparse

import myUtils
from DebugLogger import DebugLogger
from TelegramHandler import TelegramHandler
from MessageDefinition import TelegramMsg   # to be removed
from GameController import GameController

#---------------------------------------------------------------------------
## Main entry point for the Game Bot.
def main():

    myUtils.createDirectory('./log')
    myUtils.createDirectory('./cnfg')

    parser = parseCmdLine()
    options = parser.parse_args()

    print('---------------------------')
    print('Game Bot')
    myUtils.printVersionNumber()
    print('---------------------------')
    print('')

    m_logger = DebugLogger('G')
    m_gameController = GameController(m_logger)

    #if True == options.simulation:

    m_telegramHdl = TelegramHandler(1, 'TelegramHandler', 10, m_logger, m_gameController)

    # Setup phase
    m_telegramHdl.setup('./cnfg/botId.txt',
                        './cnfg/registeredIds.txt',
                        './cnfg/notificationIds.txt',
                        './cnfg/adminId.txt')

    # Start all treads
    m_telegramHdl.start()

    m_logger.logText('Successfully booted')

    m_telegramHdl.putMsgToQueue(TelegramMsg(
        TelegramMsg.Msg.BootMsg,
        'Game Bot\n\n' +
        '  Version: ' + myUtils.getVersionNumber() + '\n' +
        '  RunTime:\n'))

    exitFlag = False
    while not exitFlag:
            time.sleep(60)
    return 0


#---------------------------------------------------------------------------
## Defines the command line arguments
def parseCmdLine():
    ''' Parse the command line. '''
    parser = argparse.ArgumentParser(description='Telegram Game')
    parser.add_argument('-s', '--simulation', default=False, action='store_true',
                        help='Start with IOHandlerSim (TCP)')
    return parser


#---------------------------------------------------------------------------
if __name__ == '__main__':
    exit(main())
