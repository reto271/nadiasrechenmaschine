#!/usr/bin/python3


import queue
import threading
import asyncio
import abc
import time
from TaskBase import TaskBase


class TaskMsgQueue (TaskBase):
    def __init__(self, taskID, taskName, msgQueueSize, debugLogger):
        super(TaskMsgQueue, self).__init__(taskID, taskName, debugLogger)
        self.messageQueue = queue.Queue(msgQueueSize)
        self.messageQueueMutex = threading.Lock()
        self.msgEvent = threading.Event()
        self.exitFlag = False
        self.m_logger = debugLogger

    def putMsgToQueue(self, msg):
        self.messageQueueMutex.acquire()
        self.messageQueue.put(msg)
        self.messageQueueMutex.release()
        self.m_logger.logText('put msg to task msg queue, type: ' + str(type(msg)))
        self.msgEvent.set()

    @abc.abstractmethod
    def processMsg(self, msg):
        pass

    def terminate(self):
        self.exitFlag = True
        self.terminateMsg()

    @abc.abstractmethod
    def terminateMsg(self):
        pass

    def run(self):
        self.exitFlag = False
        while not self.exitFlag:

            # Wait for the next message
            self.msgEvent.wait()
            self.msgEvent.clear()

            self.messageQueueMutex.acquire()
            if not self.messageQueue.empty():
                msg = self.messageQueue.get()
                self.messageQueueMutex.release()
                self.m_logger.logText('Task: ' + self.m_taskName + ' is processing: ' + str(msg))
                self.processMsg(msg)
            else:
                self.messageQueueMutex.release()
                time.sleep(1)
                assert False
        self.m_logger.logText('Terminated task: "' + self.m_taskName + '"')
