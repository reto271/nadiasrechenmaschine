#!/usr/bin/python3

import datetime
import random
import myUtils
from enum import Enum


## Gamer
#
# A single gamer plays his own game
class Gamer():

    class GameState(Enum):
        Startup = 0
        WaitForResponse = 1

    ## Constructor
    def __init__(self, userId, firstName, lastName, userName, debugLogger):
        self.m_logger = debugLogger
        self.m_userId = userId
        self.m_name = 'Nobody'
        self.m_calc = '1 + 1 = ?'
        self.m_expectedResponse = '2'
        self.m_correctInRow = 0
        if '' != firstName:
            self.m_name = firstName
        elif '' != userName:
            self.m_name = userName
        elif '' != lastName:
            self.m_name = lastName
        self.m_state = Gamer.GameState.Startup

    # ------------------------------------------------------------------------------
    def getUserId(self):
        self.m_logger.logText('My user id is: ' + str(self.m_userId))
        return self.m_userId

    # ------------------------------------------------------------------------------
    def getName(self):
        return self.m_name

    # ------------------------------------------------------------------------------
    def play(self, command):
        response = 'Leider nicht, versuch es noch einmal!\n\n' + self.m_calc
        self.m_logger.logText('game command: ' + str(command) + ' state: ' + str(self.m_state))

        if (Gamer.GameState.Startup == self.m_state):
            self.m_state = Gamer.GameState.WaitForResponse
            response = ('Sali Gian\nIch freue mich mit Dir zu rechnen.\nLiebe Grüsse sendet\nNadia\n\nZum Starten ganz einfach:\n\n' + self.m_calc)

        elif (Gamer.GameState.WaitForResponse == self.m_state):
            if (self.m_expectedResponse == command):
                self.m_correctInRow = self.m_correctInRow + 1
                self.generateCalculation()
                self.m_logger.logText('self.m_correctInRow: ' + str(self.m_correctInRow))
                if (5 == self.m_correctInRow):
                    response = 'Sehr gut Gian!' + '\n\n' + self.m_calc
                elif (10 == self.m_correctInRow):
                    response = '10 Rechnungen in Folge richtig. Das machst Du super Gian.\nLiebe Grüsse Nadia\n\n' + self.m_calc
                else:
                    response = 'Bravo!' + '\n\n' + self.m_calc
            else:
                self.m_correctInRow = 0
                self.m_logger.logText('self.m_correctInRow: ' + str(self.m_correctInRow))

        else:
            response = 'aborted'
        return response

    # ------------------------------------------------------------------------------
    def generateCalculation(self):
        type = random.randint(1,101)
        if (type < 45):
            #sum
            v1 = random.randint(1,40)
            v2 = random.randint(1,50 - v1)
            self.m_expectedResponse = str(v1 + v2)
            self.m_calc = str(v1) + ' + ' + str(v2) + ' = ?'
        elif (type < 90):
            #diff
            v1 = random.randint(1,50)
            v2 = random.randint(1,v1)
            self.m_expectedResponse = str(v1 - v2)
            self.m_calc = str(v1) + ' - ' + str(v2) + ' = ?'
        else:
            #double
            v1 = random.randint(1,25)
            self.m_expectedResponse = str(2 * v1)
            self.m_calc = 'Verdopple : ' + str(v1)
