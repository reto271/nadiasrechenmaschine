import sys
import os

# ------------------------------------------------------------------------------
# Try if it is an int and return a default value
def tryInt(s, val=-1):
  try:
    return int(s)
  except ValueError:
    return val


#---------------------------------------------------------------------------
## Prints the version number
def getVersionNumber():
    try:
        with open('VersionNumber.txt', 'r') as versionFile:
            versionNumber = versionFile.read().rstrip()
            versionFile.close()
            return str(versionNumber)
    except:
        print('  File ' + str(logFileName) + ' not found.')
        return None


#---------------------------------------------------------------------------
## Prints the version number
def printVersionNumber():
    versionNumber = getVersionNumber()
    if (versionNumber):
        print('  Version: ' + str(versionNumber))


# ------------------------------------------------------------------------------
# Check and eventually create directory
def createDirectory(dirName):
    if not os.path.exists(dirName):
        os.makedirs(dirName)
