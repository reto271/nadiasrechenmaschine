#!/usr/bin/python3

import datetime
import myUtils
from Gamer import Gamer

## Game Controller
#
# Ensures that there is enough time between changing direction.
class GameController():

    ## Constructor
    def __init__(self, debugLogger):
        self.m_logger = debugLogger
        self.m_gamer = []

    # ------------------------------------------------------------------------------
    #
    def request(self, userId, command, firstName, lastName, userName):
        for gamer in self.m_gamer:
            if userId == gamer.getUserId():
                self.m_logger.logText('User found: ' + str(userId) + ', ' + gamer.getName())
                return gamer.play(command)

        self.m_logger.logText('Create new user with id: ' + str(userId))
        gamer = Gamer(userId, firstName, lastName, userName, self.m_logger)
        self.m_gamer.append(gamer)
        return gamer.play(command)
