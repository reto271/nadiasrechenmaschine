# import sys
# import os
# import time
# import random
import datetime
# import telepot
#
# from gpiozero import Button
# from gpiozero import LED
# from time import sleep
#
# # My modules
# import myUtils
# import ProjectVersion
# from UserListHandler import UserListHandler
# from DoorStatistics import DoorStatistics
# from ConfigHandler import ConfigHandler

# ------------------------------------------------------------------------------
## Logger for time taged output
class DebugLogger():
    def __init__(self, prefix):
        self.m_prefix = prefix

    def logMessageCommandReceived(self, firstName, lastName, userName, usrId, text):
        print (str(datetime.datetime.now()) + ' : ' + self.m_prefix + ' : ' +
               'Request [' + firstName + ' | ' + lastName + ' | ' + userName + '] ' +
               str(usrId) + ' : ' + text)

    def logMessageWithUserId(self, usrId, text):
        print (str(datetime.datetime.now()) + ' : ' + self.m_prefix + ' : ' +
               str(usrId) + ' : ' + text)

    def logText(self, text):
        print (str(datetime.datetime.now()) + ' : ' + self.m_prefix + ' : ' +
               text)

    def logMultiLineText(self, userId, text):
        print (str(datetime.datetime.now()) + self.m_prefix + ' : ' +
               ' : ' +str(userId) + ' >>>\n' +
               text + '\n<<<\n')
